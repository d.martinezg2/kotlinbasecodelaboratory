package com.example.nativeprototype

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private lateinit var buttonMainActivity: Button;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonMainActivity = findViewById(R.id.button_main);
        buttonMainActivity.setOnClickListener { goToLogin() }
    }
    private fun goToLogin(){
        val signUp = Intent(this, LoginActivity::class.java)
        startActivity(signUp)
    }
}