package com.example.nativeprototype

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LoginActivity : AppCompatActivity() {

    private lateinit var buttonLogin: Button;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        buttonLogin = findViewById(R.id.button_login);
        buttonLogin.setOnClickListener { goToProfile() }
    }

    private fun goToProfile(){
        val signUp = Intent(this, ProfileActivity::class.java)
        startActivity(signUp)
    }
}