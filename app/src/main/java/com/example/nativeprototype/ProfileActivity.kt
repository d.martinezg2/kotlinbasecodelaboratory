package com.example.nativeprototype

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ProfileActivity : AppCompatActivity() {

    private lateinit var buttonProfile: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        //buttonProfile = findViewById(R.id.button_profile)
        //buttonProfile.setOnClickListener { goToHome() }
    }

    private fun goToHome(){
        val signUp = Intent(this, MainActivity::class.java)
        startActivity(signUp)
    }
}