# Kotlin Base code laboratory

This is the base code to kotlin laboratory

In this lab you will learn
1. Use Figma 
2. Create a view
3. Launch a new activity
4. Configure your themes to dark and light mode
5. Understand how to change from digital prototype to native prototype

To clone de repo:

`git clone https://gitlab.com/v.parrac/kotlinbasecodelaboratory/`

To check the solution code
```
cd kotlinbasecodelaboratory
git checkout solution_code
```
